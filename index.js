const form = document.querySelector('form');
const perf = document.querySelector('#perf');
const output = document.querySelector('#output');
let calculating = false;

function timeit(name, times, fn, ...args) {
  const startTime = performance.now();
  const output = { name, selfTime: 0 };
  for (let i = 0; i < times; i++) {
    const [res, dt] = fn(...args);
    output.selfTime += dt;
    output.result = res;
  }
  output.time = performance.now() - startTime;
  return output;
}

const loaders = {
  async bigInt() {
    const { factorial } = await import('./js/big-int.js');
    return factorial;
  },
  async typedArray() {
    const { factorial } = await import('./js/typed-array.js');
    return factorial;
  },
  async goWasm() {
    await import('./go-wasm/wasm_exec.js');
    const go = new window.Go();
    const result = await WebAssembly.instantiateStreaming(
      fetch('go-wasm/main.wasm'),
      go.importObject,
    );
    go.run(result.instance);
    return window['factorial.go'];
  },
  async ocamlJs() {
    await import('./ocaml-js/factorial.bc.js');
    const fn = window['factorial.ocaml'];
    return (...args) => {
      const [code, ...rest] = fn(...args);
      if (code) throw code;
      return rest;
    };
  },
  async ocamlMelange() {
    const { factorial } = await import('./ocaml-melange/factorial.js');
    return factorial;
  },
  async rustWasm() {
    const { factorial, default: init } = await import('./rust-wasm/bigint.js');
    await init();
    return factorial;
  },
};
Object.keys(loaders).forEach((key) => {
  const value = loaders[key];
  loaders[key] = (cache => () => cache)(value());
});

async function calculate(input, times) {
  output.textContent = '';
  while (perf.firstChild) perf.firstChild.remove();
  let expected;
  perf.append(
    JSX.createElement('div', { className: 'font-bold' }, 'Provider'),
    JSX.createElement('div', { className: 'font-bold' }, 'Call Time'),
    JSX.createElement('div', { className: 'font-bold' }, 'Self Time'),
    JSX.createElement('div', { className: 'font-bold' }, 'Result'),
  );
  for (const args of [
    ['Native BigInt', times, await loaders.bigInt(), input],
    ['JS', times, await loaders.typedArray(), input],
    ['Go WASM', times, await loaders.goWasm(), input],
    ['OCaml JS', times, await loaders.ocamlJs(), input],
    ['OCaml Melange', times, await loaders.ocamlMelange(), input],
  ]) {
    await new Promise(requestAnimationFrame);
    const { name, time, selfTime, result } = timeit(...args);
    if (!expected) {
      expected = result;
      output.textContent = `${input}! = ${expected}`;
    }
    perf.append(
      JSX.createElement('div', {}, name),
      JSX.createElement('div', {}, ~~time, 'ms'),
      JSX.createElement('div', {}, ~~selfTime, 'ms'),
      JSX.createElement('div', {
        className: `${result !== expected ? 'text-red-500' : 'text-green-500'}`,
        innerHTML: result === expected ? '&#x2713;' : '&#x2717;',
      }),
    );
  }
}

function start() {
  form.addEventListener('submit', (e) => {
    e.preventDefault();
    if (calculating) return;
    calculating = true;
    const data = {};
    for (const el of e.target.elements) {
      data[el.name] = el.value;
    }
    calculate(+data.input, +data.times).finally(() => {
      calculating = false;
    });
  });
}

start();
