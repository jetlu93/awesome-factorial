export function factorial(n) {
  const startTime = performance.now();
  let result = 1n;
  for (let i = 1; i <= n; i += 1) {
    result *= BigInt(i);
  }
  return [result.toString(), performance.now() - startTime];
}
